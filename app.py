from flask import render_template, Flask
import connexion
import psycopg2


def create_table():
    try:
        conn = psycopg2.connect(database="postgres", user="postgres", password="000000", host="some-postgres", port="5432")
        cur = conn.cursor()
        cur.execute('''CREATE TABLE SHORTURL
                    (ID INT GENERATED ALWAYS AS IDENTITY,
               URL text ,
               SHORTURL text
               );''')
        print ("Table created successfully")
        conn.commit()
        conn.close()
    except Exception as e:
        print(e)

app = connexion.App(__name__, specification_dir='./')
app.add_api('swagger.yml')

create_table()

@app.route('/')
def index():
    return "Server working..."

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
    # app.run(debug=True)
