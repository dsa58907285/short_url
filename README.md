# 需求:
利用Python+Flask 實作縮短網址功能，並利用Docker製作容器方便攜帶。

# 想法:
- 規劃2支API:

    1.將網址進行縮短的API:
        利用資料庫的primary key建立唯一值ID，利用ID不重複的性質進行編碼當作網址的KEY值，當有網址需要縮短時，則把網址儲存至資料庫中並返回轉碼過 的ID給使用者，如果網址已存在於資料庫中，則直接返回已經編碼好的ID，就不會再儲存一次至資料庫中。

    2.將縮短的網址進行轉碼API:
    使用者輸入縮短的網址後，會得到轉碼後的ID，再到資料庫中找出對應的網址，將網址利用瀏覽器直接打開。

- 利用Docker來製作容器，方便在不同的環境下執行。
# 實作:
- 實作環境:

        系統:windows 10
        程式語言：python3.6+Flask
        API文件：swagger
        資料庫：postgresql

- 規劃資料庫:資料庫的部分使用postgresql，利用Docker直接下載使用<br>
建立資料表

        cur.execute('''CREATE TABLE SHORTURL
                    (ID INT GENERATED ALWAYS AS IDENTITY,
                    URL text ,
                    SHORTURL text
                    );''')
        ID:網址的KEY值
        URL:儲存使用者輸入的網址
        SHORTURL:儲存編碼後的ID值，讓使用者能夠回推網址

- 撰寫API:

#將網址進行縮短

    def SHORT_URL(get_url):
        get_url=get_url['url']
        #搜尋是否有儲存過
        conn = psycopg2.connect(database="postgres", user="postgres", password="000000", host="some-postgres", port="5432")
        cur = conn.cursor()
        cur.execute("SELECT * FROM SHORTURL where URL= '%s'"%(get_url))
        rows = cur.fetchall()
        if len(rows)==0:     #未儲存過則進行編碼動作
            cur.execute("SELECT max(id) from SHORTURL")
            row = list(cur.fetchone())
            if row[0]==None:
                row=0
            else:
                row=row[0]
            print("row:",row)
            url = short_url.encode_url(row+1)   #將ID進行轉碼
            SHORTURL='http://127.0.0.1:1000/RETURN_URL?url='+url
            cur.execute("INSERT INTO SHORTURL (URL,SHORTURL) VALUES('%s','%s')"%(get_url,url))
            conn.commit()
            conn.close()
            data={"SHORTURL":SHORTURL}
            return data
        else: #已儲存過則直接返為已前編碼的網址
            conn.close()
            data={"SHORTURL":'http://127.0.0.1:1000/RETURN_URL?url='+rows[0][2]}
            return data



#將縮短的網址進行轉碼

    def RETURN_URL(url):
        conn = psycopg2.connect(database="postgres", user="postgres", password="000000", host="some-postgres", port="5432")
        cur = conn.cursor()
        cur.execute("SELECT * FROM SHORTURL where SHORTURL= '%s'"%(url))
        rows = cur.fetchall()[0][1]
        data={"RETURN_URL":rows}
        return redirect(rows, code=302)


# docker:<br>
將製作好的程式打包成容器，在打包前需要準備下面兩個檔案，並和程式放在同一個目錄中。

#需製作一個Dockerfile儲存以下內容:

    FROM python:3.6-slim                    # 使用官方的 Python 執行環境作為基本的 Docker 影像
    WORKDIR /app                            # 設定工作目錄為 /app
    ADD . /app                              # 複製目前目錄下的內容，放進 Docker 容器中的 /app
    RUN pip install -r requirements.txt     # 安裝 requirements.txt 中所列的必要套件
    EXPOSE 5000                             # 讓 80 連接埠可以從 Docker 容器外部存取
    ENV NAME World                          # 定義環境變數
    CMD ["python", "app.py"]                # 當 Docker 容器啟動時，自動執行 app.py


#建立requirements.txt:儲存程式中需要用到的套件

    flask
    connexion
    psycopg2
    short_url


#製作容器

    docker build -t short_url:1.0 

#創建網橋:創建一個連接器

    docker network create xxxxxx

#連接網橋:將其他容器加入到剛剛創建好的網橋中，才能進行連線

    docker network connect xxxxxx ironman  <<<(ironman:要加入容器的NETWORK ID)


#執行:最後執行已製作好的容器
    
    docker run -p 1000:5000 --network=short_network --name bbb short_url:1.0 



# 執行結果
- 輸入網址:輸入需要縮短的網址，並回傳縮短後的網址

![](https://gitlab.com/dsa58907285/short_url/-/raw/master/image/INSTER_URL.PNG)



- 執行成果:在瀏覽器中輸入剛剛回傳的結果，就可直接開啟原本的網頁

![](https://gitlab.com/dsa58907285/short_url/-/raw/master/image/RETURN_URL.PNG)







# 小結:

嘗試撰寫網址縮短程式，並利用這次機會來練習docker製作容器，順便將練習過程記錄下來，這是我第一次接觸docker儘管過程跌跌撞撞的遇到很多問題，像是容器的概念、怎麼載入別人製作好的容器等等問題，很高興最終還是解決了。





